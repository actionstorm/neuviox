@echo off
if [%1] == [] goto :Usage

set Platform=%1
set DeployDir=neuvioxServerBuild
set ExeFile=neuvioxServer
set UE4Dir=UE4

set PakListFile=../../../neuviox/Build/neuvioxServer_PakList.txt
set PakFileName=neuvioxServer.pak
set PakCreatePath=../../../neuviox/Content/Paks/%PakFileName%
set PakCopyPath=..\%UE4Dir%\neuviox\Content\Paks\%PakFileName%

call Deploy.cmd
exit

:Usage
echo Usage: %~nx0 ^<Platform^>
exit

