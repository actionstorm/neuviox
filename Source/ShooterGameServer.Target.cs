// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

[SupportedPlatforms(UnrealPlatformClass.Server)]
public class ShooterGameServerTarget : TargetRules
{
	public ShooterGameServerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Server;
		bUsesSteam = true;

		ExtraModuleNames.Add("ShooterGame");
		
		bAdaptiveUnityDisablesPCH = false;
		bAdaptiveUnityCreatesDedicatedPCH = true;
		bUseXGEController = false;
		bUseBackwardsCompatibleDefaults = false;
		bIWYU = true;
		
		if (Configuration == UnrealTargetConfiguration.Shipping)
		{
			bAllowLTCG = true;
		}
		if (Configuration != UnrealTargetConfiguration.DebugGame || Configuration != UnrealTargetConfiguration.Debug)
		{
			bUseFastPDBLinking = true;
		}
	}
}
